from django.shortcuts import render, redirect
from . import forms
from .models import Jadwal_Kegiatan

def buat_Jadwal(request):
    if request.method == 'POST':
        form = forms.BuatJadwal(request.POST)
        if form.is_valid:
            form.save()
            return redirect ('jadwal:jadwal_form')
    else:
        form = forms.BuatJadwal()
        return render(request, 'buat_jadwal.html', {'form_jadwal':form})

def isi_Jadwal(request):
    if request.method=='POST':
        form = forms.BuatJadwal(request.POST)
        if form.is_valid:
            form.save()
        
    semua_jadwal = Jadwal_Kegiatan.objects.all().order_by('tanggal_kegiatan','waktu_kegiatan')
    return render(request, "hasil_form.html", {'semua_jadwal':semua_jadwal})

def hapus_Jadwal(request, id):
    jadwal_hapus = Jadwal_Kegiatan.objects.get(id=id)
    jadwal_hapus.delete()
    return redirect('jadwal:jadwal_form')