from django.db import models

class Jadwal_Kegiatan(models.Model):
    kategori = models.CharField(max_length = 100)
    nama_kegiatan = models.CharField(max_length = 100)
    tanggal_kegiatan = models.DateField(auto_now_add=False, default=None, null=True)
    waktu_kegiatan = models.TimeField(auto_now_add=False, null=True)
    tempat = models.CharField(max_length = 100)