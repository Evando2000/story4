from django import forms
from . import models

class InputDate(forms.DateInput):
    input_type = 'date'

class InputTime(forms.TimeInput):
    input_type = 'time'

class BuatJadwal(forms.ModelForm):
    class Meta:
        model = models.Jadwal_Kegiatan
        fields = ['kategori', 'nama_kegiatan', 'tanggal_kegiatan', 'waktu_kegiatan', 'tempat']
        widgets = {
            'tanggal_kegiatan' : InputDate,
            'waktu_kegiatan' : InputTime
        }