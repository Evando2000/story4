from django.urls import path
from . import views
app_name="jadwal"
urlpatterns = [
    path('', views.isi_Jadwal, name="jadwal_form"),
    path('terima', views.buat_Jadwal, name="terima"),
    path('delete/<id>', views.hapus_Jadwal, name="hapus"),
]