from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='beranda'),
    path('riwayat/', views.riwayat, name='riwayat'),
    path('fakta/', views.fakta, name='fakta'),
    path('portofolio/', views.porto, name='porto'),
    path('motivasi/', views.motivasi, name='motivasi')
]