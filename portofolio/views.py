from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, "portofolio/beranda.html")

def fakta(request):
    return render(request, "portofolio/fakta.html")

def riwayat(request):
    return render(request, "portofolio/riwayat.html")

def porto(request):
    return render(request, "portofolio/portofolio.html")

def motivasi(request):
    return render(request, "portofolio/motivasi.html")